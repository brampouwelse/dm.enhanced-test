package test.dm.enhanced.dmapi;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import test.dm.enhanced.todo.Todo;
import test.dm.enhanced.todo.TodoService;

public class TodoServiceImpl implements TodoService {

	private final List<Todo> todos = new CopyOnWriteArrayList<>();

	@Override
	public void storeTodo(Todo todo) {
		todos.add(todo);
	}

	@Override
	public List<Todo> list(String user) {
		return todos.stream().filter(t -> t.getUser().equals(user)).collect(Collectors.toList());
	}

}
