package test.dm.enhanced.dmannotation;

import org.apache.felix.dm.annotation.PropertyType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition
@PropertyType
public @interface TodoServiceConfiguration {

	String test() default "blub";

}
