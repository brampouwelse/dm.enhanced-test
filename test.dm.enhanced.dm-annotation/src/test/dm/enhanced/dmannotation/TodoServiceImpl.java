package test.dm.enhanced.dmannotation;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import org.apache.felix.dm.annotation.Component;
import org.apache.felix.dm.annotation.ConfigurationDependency;

import test.dm.enhanced.todo.Todo;
import test.dm.enhanced.todo.TodoService;

@Component
@TodoServiceConfiguration
public class TodoServiceImpl implements TodoService {

	private final List<Todo> todos = new CopyOnWriteArrayList<>();

	@Override
	public void storeTodo(Todo todo) {
		todos.add(todo);
	}

	@Override
	public List<Todo> list(String user) {
		return todos.stream().filter(t -> t.getUser().equals(user)).collect(Collectors.toList());
	}

	@ConfigurationDependency(pid="my.pid", propagate = true, required = false)
	public void updated(TodoServiceConfiguration cfg) {
		System.out.println(cfg);
	}

}
