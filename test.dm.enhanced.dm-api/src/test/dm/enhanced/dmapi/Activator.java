package test.dm.enhanced.dmapi;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

import test.dm.enhanced.todo.Todo;
import test.dm.enhanced.todo.TodoService;

public class Activator extends DependencyActivatorBase {

	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception {
		manager.add(createComponent()
				.setInterface(TodoService.class.getName(), null)
				.setImplementation(TodoServiceImpl.class)
				);

//		manager.add(createAdapterComponent()
//						.setAdaptee(TodoService.class, null)
//						.setInterface(MyAdapter.class, null)
//						.setImplementation(Object.class)
//						.setAdapteeCallbacks("add", "change", "remove", "swap")
//						);



	}



}
