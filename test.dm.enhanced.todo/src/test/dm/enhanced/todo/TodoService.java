package test.dm.enhanced.todo;
import java.util.List;

public interface TodoService {

    List<Todo> list(String user);

    void storeTodo(Todo todo);

}